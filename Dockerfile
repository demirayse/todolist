FROM golang:1.16-alpine as build

RUN mkdir -p /go/src/app
WORKDIR /go/src/app

# Copy the Go App to the container
COPY . .

# Download all the dependencies
RUN go get -d -v ./...

# Insall the package
RUN go install  -v ./...

RUN go build -o todogoapp .

EXPOSE 8000

CMD ["./todogoapp"]
