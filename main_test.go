package main

import (
	"fmt"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"encoding/json"
	"net/http"
)

// The Provider verification
/*func TestPactProvider(t *testing.T) {
	go startInstrumentedProvider()

	pact := createPact()

	// Verify the Provider - Tag-based Published Pacts for any known consumers
	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL: fmt.Sprintf("http://127.0.0.1:%d", port),
		FailIfNoPactsFound: false,
		PactURLs:                   []string{"https://ademir.pactflow.io/pacts/provider/providertodo/consumer/consumertodo/latest"},
		BrokerToken:                "_s4oGqf_0ORRSl0EeF7zCw",
		PublishVerificationResults: true,
		ProviderVersion:            "1.0.0",
		StateHandlers:              stateHandlers,
	})

	if err != nil {
		t.Fatal(err)
	}

}

var stateHandlers = types.StateHandlers{
	"check for todo": func() error {
		AllTodos = tempTodo
		return nil
	},
}

// Starts the provider API with hooks for provider states.

func startInstrumentedProvider() {
	e := echo.New()
	e.GET("/",AllTodos.GetTodos)
	e.Logger.Fatal(e.Start(":8001"))

}

// Configuration / Test Data
var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/../../pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)
var port = 8001

// Provider States data sets
var tempTodo = TodosRepo{

	MyTodos: []Todo{{"run"}},
}

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Provider:                 "Provider",
		LogDir:                   logDir,
		PactDir:                  pactDir,
		DisableToolValidityCheck: true,
		LogLevel:                 "INFO",
	}
}*/


var (
	myTempTodo Todo = Todo{Text:"wake up!"}
	tempTodos TodosRepo
	tempTodoJson = `{"text":"wake up!"}`
	actualResponseBodyByte []byte
)


func TestGetTodos (t *testing.T){
	// Set it up
	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	res := httptest.NewRecorder()

	//we make fake Context for ourselves
	c := e.NewContext(req,res)

	tempTodos.MyTodos = Todos{myTempTodo}



	if assert.NoError(t,tempTodos.GetTodos(c)){

		actualResponseBodyByte := res.Body.Bytes() //convert string json info into []byte to be able to use it in unmarshal
		var actualResponseBody TodosRepo              // --> this is where we will put the converted body info into

		json.Unmarshal(actualResponseBodyByte,&actualResponseBody.MyTodos)
		fmt.Printf("got: %s",actualResponseBody.MyTodos)

		assert.Equal(t,http.StatusOK,res.Code)
		assert.Equal(t,tempTodos.MyTodos,actualResponseBody.MyTodos)
	}

}

func TestAddTodo (t *testing.T){
	e := echo.New()

	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(tempTodoJson))
	req.Header.Set(echo.HeaderContentType,echo.MIMEApplicationJSON)
	res := httptest.NewRecorder()

	c := e.NewContext(req,res)

	tempTodos = TodosRepo{}

	if assert.NoError(t,tempTodos.AddTodo(c)){
		var actualResponseBody Todo
		actualResponseBodyByte = res.Body.Bytes()

		json.Unmarshal(actualResponseBodyByte,&actualResponseBody)

		assert.Equal(t,http.StatusCreated,res.Code)
		assert.Equal(t,myTempTodo,actualResponseBody)
	}
}