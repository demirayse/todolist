package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Todo struct {
	Text string `json:"text"`
}
type Todos []Todo

type TodosRepo struct {
	MyTodos Todos
}

var AllTodos = TodosRepo{}

func main() {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	//AllTodos.Todos = append(AllTodos.Todos, Todo{Text:"asdasdasd"})
	// Routes
	e.GET("/", AllTodos.GetTodos)
	e.POST("/",AllTodos.AddTodo)
	// Start server
	e.Logger.Fatal(e.Start(":8000"))
}

func (t *TodosRepo)GetTodos(c echo.Context) error {
	//AllTodos.Todos = append(AllTodos.Todos, Todo{Text: "Movie 1"}, Todo{Text:"asdasd"})
	response := Todos{}
	response = append(response, t.MyTodos...)

	return c.JSON(http.StatusOK, response)
}


func (t *TodosRepo) AddTodo(c echo.Context) error {
	tempTodo := Todo{}

	if err := c.Bind(&tempTodo); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	t.MyTodos= append(t.MyTodos, tempTodo)

	return c.JSON(http.StatusCreated, tempTodo)
}