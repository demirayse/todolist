module www.github.com/tayfunoztan/todogo

go 1.16

require (
	github.com/labstack/echo/v4 v4.2.1
	github.com/pact-foundation/pact-go v1.5.2
	github.com/stretchr/testify v1.4.0
)
